## BCal

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

### Deployment

`firebase deploy`

### Problem Statement

Build a MY Task page where the user can add their task on a specific date and time. 
Each task is associated with a status NOT_STARTED, IN_PROGRESS, COMPLETE.
Users can view the task added in the specific date in a calendar.
List the task for each date in a month.

### Technical Choices

##### Frontend

###### React Redux Thunk
As per requirement. Tech stack is well proven for easy and faster development. Performance benefit.

###### Material UI
Faster development and delivery of feature. Well tested components available. Open Source and vast community support. Continuous development and bug fixes done by the community.

###### Reselect
Memoizing intensive operation and preventing re calculation.

###### Moment

Out of the box utilty funtions for date operations. Faster development and delivery.

###### React Toastify

Easy to integrate notification in web app with minimal code and customization available

##### Backend

###### Firebase 

No SQL database so it is more faster. 
Quick display data in the application. 
Create Application without backend server. 

### Trade off

Usage of moment increases the build size.
Would have written test cases if time permits and would have added a feature to update status of task.


### Links

Demo: https://bcal5174.web.app. 

GitHub: https://github.com/ankit5174




 

