import * as firebase from 'firebase';

// Add the Firebase products that you want to use
import 'firebase/auth';
import 'firebase/firestore';

const firebaseConfig = {
    apiKey: process.env.REACT_APP_FIREBASE_API,
    authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.REACT_APP_FIREBASE_DATABASE_URL,
    projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
    storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.REACT_APP_FIREBASE_APP_ID,
};

firebase.initializeApp(firebaseConfig);
let db = firebase.firestore();

const FirebaseEnum = {
    COLLECTION_TASK: 'task',
    TASK_FIELD_DATE: 'date',
};

export const _addTask = (data) => {
    return db.collection(FirebaseEnum.COLLECTION_TASK).doc().set(data);
};

export const _fetchTasks = (filters) => {
    return db
        .collection(FirebaseEnum.COLLECTION_TASK)
        .where(FirebaseEnum.TASK_FIELD_DATE, '>', filters.startTimeStamp)
        .where(FirebaseEnum.TASK_FIELD_DATE, '<', filters.endTimeStamp)
        .get()
        .then(function (querySnapshot) {
            let data = [];
            querySnapshot.forEach(function (doc) {
                data.push(doc.data());
            });
            return data;
        });
};
