import queryString from 'query-string';
import { toast } from 'react-toastify';
import ToastStatusEnum from '../constants/ToastStatusEnum';

export const _getQueryParamValue = (queryParam) => {
    const queryParams = queryString.parse(window.location.search);
    return queryParams[queryParam];
};

export const _isDevelopmentMode = () => {
    return process.env.NODE_ENV === 'development';
};

export const _isProductionMode = () => {
    return process.env.NODE_ENV !== 'development';
};

export const _toast = (toastData) => {
    let options = {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: 5000,
    };
    if (toastData.autoClose !== undefined && toastData.autoClose !== null) {
        options.autoClose = toastData.autoClose;
    }
    switch (toastData.type) {
        case ToastStatusEnum.Success:
            return toast.success(toastData.message, options);
        case ToastStatusEnum.Warning:
            return toast.warn(toastData.message, options);
        case ToastStatusEnum.Error:
            return toast.error(toastData.message, options);
        case ToastStatusEnum.Info:
            return toast.info(toastData.message, options);
        default:
            break;
    }
};

export const _breakArrayIntoBlocksOf = (blockLength, data) => {
    let blocks = [];
    let block = [];
    for (let i = 0; i < data.length; i++) {
        if ((i + 1) % blockLength === 0) {
            block.push(data[i]);
            blocks.push(block);
            block = [];
        } else {
            block.push(data[i]);
        }
    }
    if (block.length > 0) {
        blocks.push(block);
    }
    return blocks;
};

export const _to = (promise) => {
    return promise
        .then((response) => [response, undefined])
        .catch((error) => Promise.resolve([undefined, error.response]));
};
