export const _getLSValueFor = (key) => {
    return localStorage.getItem(key);
};

export const _setLSValue = (key, value) => {
    localStorage.setItem(key, value);
};

export const _removeLSValue = (key) => {
    localStorage.removeItem(key);
};

export const _clearLS = () => {
    localStorage.clear();
};
