import React, { lazy, Suspense } from 'react';
import { Redirect, Switch, Route } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {
    createMuiTheme,
    MuiThemeProvider,
    CssBaseline,
    responsiveFontSizes,
} from '@material-ui/core';
import RoutesEnum from './constants/RoutesEnum';
import Index from './components/common/Loader';
import './assets/fontawesome';

const DashboardPage = lazy(() => import('./pages/MyTaskPage'));

let theme = createMuiTheme({
    typography: {
        useNextVariants: true,
        fontFamily: ['Roboto', 'sans-serif'].join(','),
    },
    spacing: 4,
    palette: {
        primary: {
            main: '#271d3f',
            contrastText: '#fff',
        },
        secondary: {
            main: '#f66623',
            contrastText: '#fff',
        },
        background: {
            lightGrey: '#ededed',
            header: '#51446a',
            highlight: '#f1ebf9',
            select: '#FBE9E7',
            status_ip: '#e2efff',
            status_c: '#f5fff8',
            status_ns: '#fff8ea',
        },
        border: {
            status_ip: '#5da8ff',
            status_c: '#25b052',
            status_ns: '#fdb913',
        },
    },
});

theme = responsiveFontSizes(theme);

function App() {
    return (
        <MuiThemeProvider theme={theme}>
            <Suspense fallback={<Index isLoading={true} />}>
                <CssBaseline />
                <ToastContainer />
                <Switch>
                    <Route
                        exact
                        path={RoutesEnum.DASHBOARD}
                        component={DashboardPage}
                    />
                    <Redirect to={RoutesEnum.DASHBOARD} />
                </Switch>
            </Suspense>
        </MuiThemeProvider>
    );
}

export default App;
