import React, { useState } from 'react';
import { TextField } from '@material-ui/core';

export const ResizeTextField = (props) => {
    const [onfocus, setOnFocus] = useState();

    const { inputProps, onFocus, ...restProps } = props;

    return (
        <TextField
            onFocus={() => {
                if (onFocus) {
                    onFocus();
                }
                setOnFocus(true);
            }}
            onBlur={() => setOnFocus(false)}
            inputProps={{
                ...inputProps,
                style: {
                    fontSize: onfocus ? '26px' : 'inherit',
                },
            }}
            {...restProps}
        />
    );
};

export default ResizeTextField;
