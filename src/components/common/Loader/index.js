import React from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import { CircularProgress } from '@material-ui/core';
import { useStyles } from './style';

export const Loader = (props) => {
    const style = useStyles();
    const { isLoading, children } = props;

    return (
        <>
            <Backdrop
                invisible={false}
                className={style.backdrop}
                open={isLoading}>
                <CircularProgress color={'secondary'} />
            </Backdrop>
            {children}
        </>
    );
};

export default Loader;
