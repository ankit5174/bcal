import React from 'react';
import { Box } from '@material-ui/core';
import classnames from 'classnames';
import { useStyles } from './style';

const Image = (props) => {
    const style = useStyles();
    const { width, height, src, imgProps, ...restPrps } = props;
    return (
        <Box width={width} height={height} {...restPrps}>
            <img
                {...imgProps}
                className={classnames(style.img)}
                alt={'Icon'}
                src={src}
            />
        </Box>
    );
};

export default Image;
