import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    img: {
        width: '100%',
        height: '100%',
    },
}));
