import React from 'react';
import Index from '../Loader';
import {Dialog, Slide} from '@material-ui/core';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const Widget = (props) => {
    const { isLoading, isFullScreen, exitFullScreen } = props;

    return (
        <div>
            <Index isLoading={isLoading}>
                {props.children}
                <Dialog
                    fullScreen
                    open={isFullScreen}
                    onClose={exitFullScreen}
                    TransitionComponent={Transition}>
                    {props.children}
                </Dialog>
            </Index>
        </div>
    );
};

export default Widget;
