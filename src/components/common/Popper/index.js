import React, { useEffect, useRef, useState } from 'react';
import {
    ClickAwayListener,
    Grow,
    Paper,
    Popper as MUIPopper,
} from '@material-ui/core';

const Popper = (props) => {
    const [open, setOpen] = useState(false);
    const anchorRef = useRef(null);
    const prevOpen = useRef(open);

    useEffect(() => {
        if (props.onClose) {
            props.onClose(handleToggle);
        }
    }, []);

    useEffect(() => {
        if (prevOpen.current === true && open === false) {
            anchorRef.current.focus();
        }

        prevOpen.current = open;
    }, [open]);

    const { placement, children, elevation = 1, action: Action } = props;

    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }
        setOpen(false);
    };

    return (
        <>
            <Action anchorRef={anchorRef} onClick={handleToggle} />
            <MUIPopper
                style={{
                    zIndex: 1600,
                }}
                placement={placement || 'bottom-start'}
                open={open}
                anchorEl={anchorRef.current}
                transition>
                {({ TransitionProps, placement }) => (
                    <Grow
                        {...TransitionProps}
                        style={{
                            transformOrigin:
                                placement === 'bottom'
                                    ? 'center top'
                                    : 'center bottom',
                        }}>
                        <Paper elevation={elevation}>
                            <ClickAwayListener onClickAway={handleClose}>
                                {children}
                            </ClickAwayListener>
                        </Paper>
                    </Grow>
                )}
            </MUIPopper>
        </>
    );
};

export default Popper;
