import { Badge, withStyles } from '@material-ui/core';

const StyledBadge = withStyles((theme) => ({
    badge: {
        border: `2px solid ${theme.palette.background.paper}`,
        padding: '0 4px',
        background: theme.palette.secondary.main,
        color: theme.palette.common.white,
    },
}))(Badge);

export default StyledBadge;
