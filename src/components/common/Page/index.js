import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import { useStyles } from './style';
import classnames from 'classnames';
import { Dialog, Slide } from '@material-ui/core';
import Loader from '../Loader';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const Page = (props) => {
    const style = useStyles();
    const {
        isLoading,
        position,
        appBarStyle,
        isFullScreen,
        exitFullScreen,
        loaderComponent: PageLoader = Loader,
        bottomToolbar,
    } = props;

    return (
        <>
            <AppBar
                position={position || 'sticky'}
                className={classnames(
                    { [appBarStyle]: appBarStyle },
                    style.appBar
                )}>
                {props.toolbar}
            </AppBar>
            <main>
                <PageLoader isLoading={isLoading} />
                {props.children}
                <Dialog
                    fullScreen
                    open={isFullScreen || false}
                    onClose={exitFullScreen}
                    TransitionComponent={Transition}>
                    {props.children}
                </Dialog>
            </main>

            <AppBar
                position="fixed"
                color="primary"
                className={style.bottomToolbar}>
                {bottomToolbar}
            </AppBar>
        </>
    );
};

export default Page;
