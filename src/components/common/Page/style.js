import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    bottomToolbar: {
        top: 'auto',
        bottom: 0,
        backgroundColor: theme.palette.common.white,
    },
}));
