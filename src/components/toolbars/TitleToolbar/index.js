import React from 'react';
import { useStyles } from './style';
import classnames from 'classnames';
import Typography from '@material-ui/core/Typography';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Box, Toolbar, IconButton } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

const TitleToolbar = (props) => {
    const style = useStyles();
    const history = useHistory();

    const { title, onBack, showBack = false } = props;

    const onBackAction = () => history.goBack();

    return (
        <Toolbar key={1} className={classnames(style.toolbar)}>
            {showBack && (
                <IconButton
                    className={classnames(style.backButton)}
                    onClick={onBack || onBackAction}>
                    <ArrowBackIcon />
                </IconButton>
            )}
            <Box ml={4} />
            <Typography noWrap variant={'h6'} align={'center'}>
                {title}
            </Typography>
        </Toolbar>
    );
};

export default TitleToolbar;
