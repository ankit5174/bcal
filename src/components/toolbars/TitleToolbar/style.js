import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    toolbar: {
        backgroundColor: theme.palette.primary.main,
    },
    backButton: {
        color: theme.palette.common.white,
    },
}));
