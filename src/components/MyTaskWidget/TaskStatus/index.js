import React, { useRef } from 'react';
import { Box } from '@material-ui/core';
import Popper from '../../common/Popper';
import TaskStatusChip from '../TaskStatusChip';
import TaskList from '../TaskList';

const TaskStatus = (props) => {
    const { status, value, tasks } = props;
    const onPopperClose = useRef();
    return (
        <Popper
            elevation={24}
            placement="right"
            onClose={(onClose) => (onPopperClose.current = onClose)}
            action={(props) => (
                <Box
                    ref={props.anchorRef}
                    onClick={(e) => {
                        e.stopPropagation();
                        props.onClick();
                    }}>
                    <TaskStatusChip status={status} value={value} />
                </Box>
            )}>
            <Box minWidth={360}>
                <TaskList
                    onClose={() => onPopperClose.current()}
                    tasks={tasks}
                />
            </Box>
        </Popper>
    );
};

export default TaskStatus;
