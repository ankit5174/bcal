import React, { useRef } from 'react';
import { Box, Typography } from '@material-ui/core';
import { useStyles } from './style';
import classnames from 'classnames';
import Popper from '../../common/Popper';
import AddTask from '../AddTask';
import TaskStatus from '../TaskStatus';

const Day = (props) => {
    const style = useStyles();
    const { date, onAddSuccess, tasks = {} } = props;
    const onPopperClose = useRef();
    return (
        <Popper
            elevation={24}
            placement="right"
            onClose={(onClose) => (onPopperClose.current = onClose)}
            action={(props) => (
                <Box
                    ref={props.anchorRef}
                    onClick={date && props.onClick}
                    justifyContent={'space-between'}
                    className={classnames({ [style.pointer]: !!date })}
                    display={'flex'}
                    minHeight={120}
                    p={4}>
                    <Typography variant={'caption'}>
                        {date && date.format('DD')}
                    </Typography>
                    <Box mt={-0.75} display={'flex'} flexDirection={'column'}>
                        {Object.keys(tasks).map((status) => (
                            <Box
                                display={'flex'}
                                justifyContent={'flex-end'}
                                key={status}
                                mb={1}
                                mt={1}>
                                <TaskStatus
                                    tasks={tasks[status]}
                                    status={status}
                                    value={`${tasks[status].length}  ${status}`}
                                />
                            </Box>
                        ))}
                    </Box>
                </Box>
            )}>
            <Box minWidth={360}>
                <AddTask
                    onAddSuccess={onAddSuccess}
                    onClose={() => onPopperClose.current()}
                    date={date}
                />
            </Box>
        </Popper>
    );
};

export default Day;
