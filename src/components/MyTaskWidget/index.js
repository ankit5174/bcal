import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { selectIsRequesting } from '../../store/request/request-selector';
import Widget from '../common/Widget';
import {
    AppBar,
    Box,
    Divider,
    IconButton,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Toolbar,
    Typography,
} from '@material-ui/core';
import FullscreenIcon from '@material-ui/icons/Fullscreen';
import FullscreenExitIcon from '@material-ui/icons/FullscreenExit';
import RefreshIcon from '@material-ui/icons/Refresh';
import { useStyles } from './style';
import moment from 'moment';
import { _breakArrayIntoBlocksOf } from '../../utils/CommonUtils';
import NavigateNextRoundedIcon from '@material-ui/icons/NavigateNextRounded';
import NavigateBeforeRoundedIcon from '@material-ui/icons/NavigateBeforeRounded';
import classnames from 'classnames';
import Day from './Day';
import TaskAction from '../../store/task/task-action';
import { selectTasks } from '../../store/task/task-selector';

const getFilters = (cal) => {
    return {
        startTimeStamp: moment(cal).startOf('month').valueOf(),
        endTimeStamp: moment(cal).endOf('month').valueOf(),
    };
};

export const MyTaskWidget = () => {
    const isLoading = useSelector((state) =>
        selectIsRequesting(state, [TaskAction.REQUEST_FETCH_TASKS])
    );
    const tasks = useSelector((state) => selectTasks(state, []));

    const dispatch = useDispatch();
    const fetchTasks = useCallback(
        (filters) => dispatch(TaskAction.fetchTasks(filters)),
        [dispatch]
    );

    const style = useStyles();
    const [isFullScreen, setIsFullScreen] = useState(false);
    const [cal, updateCal] = useState(moment());
    const [tasksByDate, setTasksByDate] = useState({});

    useEffect(() => {
        fetchTasks(getFilters(cal));
    }, [cal, fetchTasks]);
    useEffect(() => {
        let data = {};
        tasks.forEach((task) => {
            let date = moment(task.date).format('D');
            let dateTasks = data[date];
            if (!!dateTasks) {
                if (!!dateTasks[task.status]) {
                    dateTasks[task.status].push(task);
                } else {
                    dateTasks[task.status] = [task];
                }
            } else {
                data[date] = {
                    [task.status]: [task],
                };
            }
        });
        setTasksByDate(data);
    }, [tasks]);

    const onToggleFullScreen = () => {
        setIsFullScreen(!isFullScreen);
    };

    const firstDayOfMonth = () => {
        return moment(cal).startOf('month').format('d');
    };

    const daysInMonth = () => {
        let daysInMonth = [];
        for (let i = 0; i < firstDayOfMonth(); i++) {
            daysInMonth.push('');
        }
        for (let d = 1; d <= cal.daysInMonth(); d++) {
            daysInMonth.push(d);
        }
        return daysInMonth;
    };

    const onNextMonth = () => {
        cal.add(1, 'months');
        updateCal(moment(cal));
        setTasksByDate({});
    };

    const onPrevMonth = () => {
        cal.subtract(1, 'months');
        updateCal(moment(cal));
        setTasksByDate({});
    };

    const isToday = (dayCal) => {
        return moment().isSame(dayCal, 'day');
    };

    const breakDaysInMonthToWeeks = () => {
        let weeks = _breakArrayIntoBlocksOf(7, daysInMonth());
        let noOfDaysInLastWeek = weeks[weeks.length - 1].length;
        if (noOfDaysInLastWeek < 7) {
            for (let i = 0; i < 7 - noOfDaysInLastWeek; i++) {
                weeks[weeks.length - 1].push('');
            }
        }
        return weeks;
    };

    const daysInWeek = breakDaysInMonthToWeeks();

    return (
        <Widget
            isFullScreen={isFullScreen}
            exitFullScreen={onToggleFullScreen}
            isLoading={isLoading}>
            <AppBar
                elevation={0}
                className={classnames(style.appBar)}
                position={'static'}>
                <Toolbar className={classnames(style.toolbar)}>
                    <Box display={'flex'} alignItems={'center'} flex={1}>
                        <IconButton onClick={onPrevMonth}>
                            <NavigateBeforeRoundedIcon htmlColor={'white'} />
                        </IconButton>
                        <Box minWidth={200}>
                            <Typography
                                align={'center'}
                                color="inherit"
                                variant="h6">
                                {cal.format('MMMM')}, {cal.format('YYYY')}
                            </Typography>
                        </Box>
                        <IconButton onClick={onNextMonth}>
                            <NavigateNextRoundedIcon htmlColor={'white'} />
                        </IconButton>
                    </Box>
                    <IconButton onClick={() => fetchTasks(getFilters(cal))}>
                        <RefreshIcon htmlColor={'white'} />
                    </IconButton>
                    <IconButton onClick={onToggleFullScreen}>
                        {isFullScreen ? (
                            <FullscreenExitIcon htmlColor={'white'} />
                        ) : (
                            <FullscreenIcon htmlColor={'white'} />
                        )}
                    </IconButton>
                </Toolbar>
            </AppBar>
            <TableContainer>
                <Table
                    stickyHeader
                    className={style.table}
                    aria-label="simple table"
                    height={150}>
                    <TableHead>
                        <TableRow>
                            {moment.weekdaysShort().map((day) => (
                                <TableCell
                                    width={'14.28%'}
                                    className={classnames(style.hCol)}
                                    align={'center'}>
                                    {day}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {daysInWeek.map((week) => (
                            <TableRow>
                                {week.map((date, index) => (
                                    <TableCell
                                        width={'14.28%'}
                                        padding={'none'}>
                                        <Box display={'flex'}>
                                            <Box
                                                bgcolor={
                                                    isToday(
                                                        moment(cal).set(
                                                            'date',
                                                            date
                                                        )
                                                    ) && 'background.select'
                                                }
                                                flex={1}>
                                                <Day
                                                    tasks={tasksByDate[date]}
                                                    onAddSuccess={() =>
                                                        fetchTasks(
                                                            getFilters(cal)
                                                        )
                                                    }
                                                    date={
                                                        !!date
                                                            ? moment(cal).set(
                                                                  'date',
                                                                  date
                                                              )
                                                            : ''
                                                    }
                                                />
                                            </Box>
                                            {index !== 6 && (
                                                <Divider
                                                    flexItem={true}
                                                    orientation={'vertical'}
                                                />
                                            )}
                                        </Box>
                                    </TableCell>
                                ))}
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Widget>
    );
};

export default MyTaskWidget;
