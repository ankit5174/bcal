import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    title: {
        flex: '1 1 100%',
    },
    toolbar: {
        backgroundColor: theme.palette.background.header,
        color: theme.palette.common.white,
        borderTopLeftRadius: theme.spacing(2),
        borderTopRightRadius: theme.spacing(2),
    },
    appBar: {
        backgroundColor: 'transparent',
    },
    hCol: {
        backgroundColor: theme.palette.background.highlight,
    },
    p0: {
        padding: 0,
    },
}));
