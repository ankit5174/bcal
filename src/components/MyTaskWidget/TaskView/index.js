import React from 'react';
import { Box, Typography } from '@material-ui/core';
import moment from 'moment';
import { useStyles } from './style';
import classnames from 'classnames';

const TaskView = (props) => {
    const style = useStyles();
    const { task } = props;
    return (
        <Box
            p={1}
            pl={2}
            pr={2}
            borderRadius={5}
            className={classnames(
                {
                    [style[task.status]]: true,
                },
                style.chip
            )}>
            <Typography variant={'h6'}>{task.title}</Typography>
            <Typography variant={'caption'}>
                {moment(task.date).format('DD MMMM YYYY, hh:mm')}
            </Typography>
            <Typography variant={'body2'}>{task.description}</Typography>
        </Box>
    );
};

export default TaskView;
