import React from 'react';
import { Box, IconButton, Toolbar, Divider } from '@material-ui/core';
import TaskView from '../TaskView';
import Typography from '@material-ui/core/Typography';
import { useSelector } from 'react-redux';
import { selectStrings } from '../../../store/localization/localization-selector';
import CloseRoundedIcon from '@material-ui/icons/CloseRounded';

const TaskList = (props) => {
    const strings = useSelector((state) => selectStrings(state));

    const { tasks, onClose } = props;
    return (
        <>
            <Toolbar>
                <Box flex={1}>
                    <Typography variant={'h6'}>{strings.tasks}</Typography>
                </Box>
                <IconButton
                    onClick={(e) => {
                        e.stopPropagation();
                        onClose();
                    }}>
                    <CloseRoundedIcon />
                </IconButton>
            </Toolbar>
            <Divider />
            <Box p={4} pt={2} pb={2}>
                {tasks.map((task) => {
                    return (
                        <Box mt={2} mb={2}>
                            <TaskView task={task} />
                        </Box>
                    );
                })}
            </Box>
        </>
    );
};

export default TaskList;
