import React from 'react';
import { Chip } from '@material-ui/core';
import classnames from 'classnames';
import { useStyles } from './style';

const TaskStatusChip = (props) => {
    const style = useStyles();
    const { status, value } = props;
    return (
        <Chip
            className={classnames(
                {
                    [style[status]]: true,
                },
                style.chip
            )}
            size={'small'}
            label={value || status}
        />
    );
};

export default TaskStatusChip;
