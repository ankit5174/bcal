import { makeStyles } from '@material-ui/core/styles';
import AppEnum from '../../../constants/AppEnum';

export const useStyles = makeStyles((theme) => ({
    chip: {
        padding: theme.spacing(2),
        height: theme.spacing(5.5),
        fontSize: theme.spacing(2.5),
        cursor: 'pointer',
    },
    [AppEnum.TASK_STATUS_NOT_STARTED]: {
        backgroundColor: theme.palette.background.status_ns,
        border: `solid 1px ${theme.palette.border.status_ns}`,
        color: theme.palette.text.status_ns,
    },
    [AppEnum.TASK_STATUS_IN_PROGRESS]: {
        backgroundColor: theme.palette.background.status_ip,
        border: `solid 1px ${theme.palette.border.status_ip}`,
        color: theme.palette.text.status_ip,
    },
    [AppEnum.TASK_STATUS_COMPLETE]: {
        backgroundColor: theme.palette.background.status_c,
        border: `solid 1px ${theme.palette.border.status_c}`,
        color: theme.palette.text.status_c,
    },
}));
