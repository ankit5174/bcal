import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    menuPopover: {
        top: `240px !important`,
        left: `16px !important`,
    },
    desc: {
        padding: theme.spacing(2),
        underline: 'none',
    },
}));
