import React, { useRef, useState, useCallback } from 'react';
import { useSelector } from 'react-redux';
import { selectIsRequesting } from '../../../store/request/request-selector';
import Widget from '../../common/Widget';
import {
    Box,
    Button,
    Divider,
    IconButton,
    TextField,
    Toolbar,
    Select,
    InputLabel,
    FormControl,
    MenuItem,
} from '@material-ui/core';
import { selectStrings } from '../../../store/localization/localization-selector';
import ResizeTextField from '../../common/ResizeTextField';
import classnames from 'classnames';
import Typography from '@material-ui/core/Typography';
import CloseRoundedIcon from '@material-ui/icons/CloseRounded';
import { useStyles } from './style';
import moment from 'moment';
import AppEnum from '../../../constants/AppEnum';
import TaskAction from '../../../store/task/task-action';
import { useDispatch } from 'react-redux';
import ToastStatusEnum from '../../../constants/ToastStatusEnum';
import { _toast } from '../../../utils/CommonUtils';

const taskStatus = [
    AppEnum.TASK_STATUS_NOT_STARTED,
    AppEnum.TASK_STATUS_IN_PROGRESS,
    AppEnum.TASK_STATUS_COMPLETE,
];

export const AddTask = (props) => {
    const isLoading = useSelector((state) =>
        selectIsRequesting(state, [TaskAction.REQUEST_ADD_TASK])
    );
    const strings = useSelector((state) => selectStrings(state));

    const dispatch = useDispatch();
    const addTask = useCallback((data) => dispatch(TaskAction.addTask(data)), [
        dispatch,
    ]);

    const style = useStyles();
    const [form, setFormField] = useState({
        title: '',
        description: '',
        time: moment().format('hh:mm'),
        status: '',
    });
    const selectRef = useRef();

    const { date, onAddSuccess, onClose } = props;

    const onFormFieldChange = (fieldName) => (event) => {
        let value = event.target.value;
        setFormField({
            ...form,
            [fieldName]: value,
        });
    };

    const validate = () => {
        return Object.values(form).every((field) => !!field);
    };

    const onSave = () => {
        let time = form.time.split(':');
        let data = {
            ...form,
            date: date.set('hour', time[0]).set('minute', time[1]).valueOf(),
        };
        delete data['time'];
        addTask(data).then(([response, error]) => {
            if (!error) {
                if (onAddSuccess) {
                    onAddSuccess();
                }
                onClose();
                let toast = {
                    type: ToastStatusEnum.Success,
                    message: strings.taskAddedSuccessfully,
                    autoClose: true,
                };
                _toast(toast);
            }
        });
    };

    return (
        <Widget isLoading={isLoading}>
            <Box width={'100%'}>
                <Toolbar>
                    <Box flex={1}>
                        <Typography variant={'h6'}>
                            {strings.addTask}
                        </Typography>
                    </Box>
                    <IconButton onClick={onClose}>
                        <CloseRoundedIcon />
                    </IconButton>
                </Toolbar>
                <Divider />
                <Box display={'flex'} flexDirection={'column'} p={4}>
                    <ResizeTextField
                        fullWidth
                        autoFocus
                        value={form.title}
                        onChange={onFormFieldChange('title')}
                        label={strings.title}
                    />
                    <Box mt={4} display={'flex'} alignItems={'center'}>
                        {/*<QueryBuilderRoundedIcon />*/}
                        <Typography
                            style={{ fontSize: '1rem' }}
                            variant={'subtitle2'}>
                            {date.format('DD MMMM YYYY')}
                        </Typography>
                        <Box ml={4} />
                        <TextField
                            value={form.time}
                            onChange={onFormFieldChange('time')}
                            type="time"
                            defaultValue={moment().format('hh:mm')}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            InputProps={{
                                disableUnderline: true,
                            }}
                            inputProps={{
                                step: 300,
                            }}
                        />
                    </Box>
                    <Box mt={4}>
                        <FormControl
                            size={'small'}
                            variant={'outlined'}
                            fullWidth>
                            <InputLabel>{strings.status}</InputLabel>
                            <Select
                                inputProps={{ inputRef: selectRef }}
                                rootRef={selectRef}
                                MenuProps={{
                                    disablePortal: true,
                                    anchorEl: selectRef.current,
                                    PopoverClasses: {
                                        paper: style.menuPopover,
                                    },
                                }}
                                value={form.status}
                                onChange={onFormFieldChange('status')}>
                                {taskStatus.map((status) => (
                                    <MenuItem value={status}>{status}</MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Box>
                    <Box mt={4} bgcolor={'background.lightGrey'}>
                        <TextField
                            onChange={onFormFieldChange('description')}
                            inputProps={{ className: classnames(style.desc) }}
                            fullWidth
                            variant={'standard'}
                            rows={4}
                            multiline
                            InputProps={{
                                disableUnderline: true,
                            }}
                            rowsMax={4}
                            placeholder={strings.description}
                        />
                    </Box>
                    <Box mt={4} display={'flex'} justifyContent={'flex-end'}>
                        <Button
                            onClick={onSave}
                            disabled={!validate()}
                            size={'small'}
                            disableElevation
                            color={'secondary'}
                            variant={'contained'}>
                            {strings.save}
                        </Button>
                    </Box>
                </Box>
            </Box>
        </Widget>
    );
};

export default AddTask;
