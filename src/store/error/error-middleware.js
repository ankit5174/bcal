import { selectStrings } from '../localization/localization-selector';
import { toast } from 'react-toastify';
import { _toast } from '../../utils/CommonUtils';
import ToastStatusEnum from '../../constants/ToastStatusEnum';

export default (store) => (next) => (action) => {
    if (action.error) {
        let { payload } = action;
        if (payload) {
            let errorMessage =
                payload.statusText ||
                selectStrings(store.getState()).somethingWentWrong;
            _toast({
                type: ToastStatusEnum.Error,
                message: errorMessage,
                position: toast.POSITION.TOP_LEFT,
                autoClose: 2500,
            });
        }
    }
    next(action);
};
