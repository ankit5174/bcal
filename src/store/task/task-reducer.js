import TaskAction from './task-action';

const INITIAL_STATE = {
    list: [],
};

const setTasks = (state, action) => {
    return {
        ...state,
        list: action.payload,
    };
};

export default (state = INITIAL_STATE, action) => {
    if (action.error) {
        return state;
    }
    switch (action.type) {
        case TaskAction.REQUEST_FETCH_TASKS_FINISHED:
            return setTasks(state, action);
        default:
            return state;
    }
};
