import ActionUtils from '../../utils/ActionUtils';
import { _addTask, _fetchTasks } from '../../utils/FirebaseUtils';

export default class TaskAction {
    static REQUEST_ADD_TASK = 'REQUEST_ADD_TASK';
    static REQUEST_FETCH_TASKS = 'REQUEST_FETCH_TASKS';
    static REQUEST_FETCH_TASKS_FINISHED = 'REQUEST_FETCH_TASKS_FINISHED';

    static fetchTasks = (filters) => {
        return async (dispatch) => {
            let [response] = await ActionUtils.createThunkEffect(
                dispatch,
                TaskAction.REQUEST_FETCH_TASKS,
                _fetchTasks,
                filters
            );
            return response;
        };
    };

    static addTask = (data) => {
        return async (dispatch) => {
            let [response, error] = await ActionUtils.createThunkEffect(
                dispatch,
                TaskAction.REQUEST_ADD_TASK,
                _addTask,
                data
            );
            return [response, error];
        };
    };
}
