import { createSelector } from 'reselect';

export const selectIsRequesting = createSelector(
    (state) => state.request,
    (state, actionTypes) => actionTypes,
    (requestState, actionTypes) => {
        return actionTypes.some((actionType) => requestState[actionType]);
    }
);
