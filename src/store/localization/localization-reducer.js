import { selectLanguage } from '../../assets/strings';

const INITIAL_STATE = {
    strings: selectLanguage('en'),
};
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        default:
            return state;
    }
};
