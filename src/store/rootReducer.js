import { combineReducers } from 'redux';
import localizationReducer from './localization/localization-reducer';
import requestReducer from './request/request-reducer';
import errorReducer from './error/error-reducer';
import taskReducer from './task/task-reducer';

const appReducers = combineReducers({
    localization: localizationReducer,
    request: requestReducer,
    error: errorReducer,
    tasks: taskReducer,
});

//can be enhanced for login/logout scenarios
const rootReducer = (state, action) => {
    switch (action.type) {
        default:
            return appReducers(state, action);
    }
};

export default rootReducer;
