import React from 'react';
import Page from '../../components/common/Page';
import { useSelector } from 'react-redux';
import { selectIsRequesting } from '../../store/request/request-selector';
import TitleToolbar from '../../components/toolbars/TitleToolbar';
import { selectStrings } from '../../store/localization/localization-selector';
import MyTaskWidget from '../../components/MyTaskWidget';
import { Box, Paper } from '@material-ui/core';

export const MyTaskPage = () => {
    const isLoading = useSelector((state) => selectIsRequesting(state, []));
    const strings = useSelector((state) => selectStrings(state, []));

    return (
        <Page
            toolbar={[<TitleToolbar title={strings.myTask} />]}
            isLoading={isLoading}>
            <Box p={4}>
                <Paper>
                    <MyTaskWidget />
                </Paper>
            </Box>
        </Page>
    );
};

export default MyTaskPage;
