export const selectLanguage = (local) => {
    return strings[local];
};

const strings = {
    en: {
        myTask: 'My Task',
        somethingWentWrong: 'Something Went Wrong',
        title: 'Title',
        addTask: 'Add Task',
        time: 'Time',
        date: 'Date',
        status: 'Status',
        save: 'save',
        description: 'Description',
        taskAddedSuccessfully: 'Task Added Successfully',
        tasks: 'Tasks',
    },
    mr: {},
};
